package game;

import java.awt.event.KeyEvent;

import game.input.Controller;

public class Game {
	public Controller controls;

	public int time = 0;
	public Game() {
		controls = new Controller();
	}

	public void tick(boolean[] key) {
		time++;
		boolean forward = key[KeyEvent.VK_W];
		boolean back = key[KeyEvent.VK_S];
		boolean left = key[KeyEvent.VK_A];
		boolean right = key[KeyEvent.VK_D];
		boolean jump = key[KeyEvent.VK_SPACE];
		boolean crouch = key[KeyEvent.VK_CONTROL];
		boolean run = key[KeyEvent.VK_SHIFT];
		controls.tick(forward, back, left, right, jump, crouch, run);
	}
}
// int anim = (int) (Math.sin((System.currentTimeMillis() + i * 2) % 2000.0 /
// 1000 * Math.PI * 2) * 100);
// int anim2 = (int) (Math.cos((System.currentTimeMillis() + i * 2) % 2000.0 /
// 1000 * Math.PI * 2) * 100);