package game.input;

public class Controller {

	public double x, y, z, rotation, xa, za, rotationa;
	public static boolean turnLeft = false;
	public static boolean turnRight = false;
	public static boolean walk = false;
	public static boolean crouchWalk = false;
	public static boolean runWalk = false;

	public void tick(boolean forward, boolean back, boolean left, boolean right, boolean jump, boolean crouch, boolean run) {
		double rotationSpeed = 0.008;
		double playerSpeed = 0.8;
		double jumpHeight = 0.5;
		double crouchHeight = 0.3;
		double xMove = 0;
		double zMove = 0;
		
		
		if (forward) {
			zMove = playerSpeed;
			walk = true;
		}

		if (back) {
			zMove = -playerSpeed;
			walk = true;
		}

		if (left) {
			xMove = -playerSpeed;
			walk = true;
		}

		if (right) {
			xMove = playerSpeed;
			walk = true;
		}

		if (turnLeft) {
			rotationa -= rotationSpeed;
		}

		if (turnRight) {
			rotationa += rotationSpeed;
		}

		if (jump) {
			y += jumpHeight;
		}

		if (crouch) {
			y -= crouchHeight;
			run = false;
			walk = false;
			crouchWalk = true;
			playerSpeed =0.2;
		} else {
			crouchWalk = false;
		}

		if (run) {
			playerSpeed = 1.4;
			walk = true;
			runWalk = true;
		}
		
		if (!forward && !back && !left && !right && !turnRight && !turnLeft){
			walk = false;
		}
		if (!crouch){
			crouchWalk = false;
		}
		if(!run){
			runWalk = false;
		}
		xa += (xMove * Math.cos(rotation) + zMove * Math.sin(rotation)) * playerSpeed;
		za += (zMove * Math.cos(rotation) - xMove * Math.sin(rotation)) * playerSpeed;

		x += xa;
		y *= 0.9;
		z += za;
		xa *= 0.1;
		za *= 0.1;
		rotation += rotationa;
		rotationa *= 0.8;
	}
}